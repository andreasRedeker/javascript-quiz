var names = ["Lucas", "Linus", "Ida", "Lisa", "Kira", "Jochen", "Robert", "Luke",
    "Christoph", "Tim", "Thomas", "David", "Sophie", "Daniel", "Maurice", "Kerstin", "Andreas", "Anabel", "Adam"];

var questions = ["Frage 1", "Frage 2", "Frage 3", "Frage 4", "Frage 5", "Frage 6", "Frage 7", "Frage 7", "Frage 8",
    "Frage 9", "Frage 10", "Frage 11", "Frage 12", "Frage 13", "Frage 14", "Frage 15", "Frage 16", "Frage 17",
    "Frage 18", "Frage 19"];

function displayName() {
    var randomName = names[Math.floor(Math.random() * names.length)];
    var randomQuestion = questions[Math.floor(Math.random() * questions.length)];
    document.getElementById("name").innerHTML = randomName;
    document.getElementById("question").innerHTML = randomQuestion;
    var index = names.indexOf(randomName);
    var index1 = questions.indexOf(randomQuestion);
    names.splice(index, 1);
    questions.splice(index1, 1);// Aktueller Name wird aus Array gelöscht
    if (names.length === 0 || questions.length === 0) {
        document.getElementById("name").innerHTML = "The End.";
        document.getElementById("question").innerHTML = " ";
        document.getElementById("button").classList.add('hidden');
        document.getElementById("reload").classList.remove('hidden');
    }
}

function reload() {
    location.reload();
}
